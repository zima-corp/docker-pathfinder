FROM 6run0/pathfinder-base:latest

#ARG PATHFINDER_REPO=https://github.com/goryn-clade/pathfinder.git
#ARG PATHFINDER_REPO=https://github.com/MonoliYoda/pathfinder.git
ARG PATHFINDER_REPO=https://github.com/NoxideLive/pathfinder.git

COPY . /

RUN \
  set -eux; \
  git config --global http.version HTTP/1.1; \
  git config --global url."https://github.com/".insteadOf git@github.com: ;\
  git clone  --single-branch --depth 1 ${PATHFINDER_REPO} .; \
  git apply /patches/firebase-jwt-jwt-decode.patch; \
  env COMPOSER_ALLOW_SUPERUSER=1 composer install \
  --ansi \
  --no-dev \
  --no-interaction \
  --no-progress \
  --optimize-autoloader \
  --prefer-dist \
  ; \
  rm -rf .git; \
  rm -rf ~/.cache; \
  rm -rf ~/.composer

VOLUME $APPLICATION_DIR/history
VOLUME $APPLICATION_DIR/logs
VOLUME $APPLICATION_DIR/tmp
VOLUME $APPLICATION_DIR/conf

EXPOSE 80
