; Pathfinder Config

[PATHFINDER]
; Name of installation
;           This can be changed to any name
;           This name is used in e.g. emails, user interface
; Syntax:   String
; Default:  Pathfinder
NAME                        =   %%PATHFINDER_NAME%%

; Pathfinder version
;           Version number should not be changed manually.
;           Version is used for CSS/JS cache busting and is part of the URL for static resources:
;           e.g. public/js/vX.X.X/app.js
; Syntax:   String (current version)
; Default:  v2.2.3
VERSION                     =   %%PATHFINDER_VERSION%%

; Contact information [optional]
;           Shown on 'licence', 'contact' page.
; Syntax:   String
; Default:  https://github.com/exodus4d
CONTACT                     =   %%PATHFINDER_CONTACT%%

; Public contact email [optional]
; Syntax:   String
; Default:
EMAIL                       =

; Repository URL [optional]
;           Used for 'licence', 'contact' page.
; Syntax:   String
; Default:  https://github.com/exodus4d/pathfinder
REPO                        =   %%PATHFINDER_REPO%%

; Show warning on 'login' page if /setup route is active
;           DO NOT disable this warning unless /setup route is protected or commented in routes.ini
; Syntax:   0 | 1
; Default:  1
SHOW_SETUP_WARNING          =   0

; Show full login page
;           If disabled, some section don´t appear:
;           'Slideshow', 'Features', 'Admin', 'Install', 'About'
; Syntax:   0 | 1
; Default:  1
SHOW_COMPLETE_LOGIN_PAGE    =   1

; REGISTRATION ====================================================================================
[PATHFINDER.REGISTRATION]
; Registration status (for new users)
;           If disabled, users can no longer register a new account on this installation.
; Syntax:   0 | 1
; Default:  1
STATUS                      =   1

[PATHFINDER.LOGIN]
; Expire time for login cookies
;           Login Cookie information send by clients is re-validated by the server.
;           The expire time for each cookie is stored in DB. Expired Cookies become invalid.
; Syntax:   Integer (days)
; Default:  30
COOKIE_EXPIRE               =   30

; Show 'scheduled maintenance' warning
;           If enabled, active users will see a notification panel.
;           This can be used to inform users about upcoming maintenance shutdown.
;           This flag can be enabled "on the fly" (no page reload required to see the notice).
; Syntax:   0 | 1
; Default:  0
MODE_MAINTENANCE            =   0

; Share maps between logged in characters in the same session
;           If enabled this will grant the active character access to any maps 
;           that other characters from the "switch character" menu have access to
; Syntax    0 | 1
; Default   0
SESSION_SHARING             =  0

; Login restrictions (white lists)
;           Login/registration can be restricted to specific groups.
;           Use comma separated strings for CCP Ids (e.g. 1000166,1000080).
;           If no groups are specified, all characters are allowed.
; Syntax:   String (comma separated)
; Default:
CHARACTER                   = %%PATHFINDER_CHARACTER%%
CORPORATION                 = %%PATHFINDER_CORPORATION%%
ALLIANCE                    = %%PATHFINDER_ALLIANCE%%

[PATHFINDER.CHARACTER]
; Auto location select for characters
;           If enabled, characters can activate the "auto location select" checkbox in their account settings.
;           If checkbox active, solar systems get auto selected on map based on their current system.
; Hint:     This can increase server load because of more client requests.
; Syntax:   0 | 1
; Default:  1
AUTO_LOCATION_SELECT        =   1

; Slack API integration ===========================================================================
[PATHFINDER.SLACK]
; Slack API status
;           This is a global toggle for all Slack related features.
;           Check PATHFINDER.MAP section for individual control.
; Syntax:   0 | 1
; Default:  1
STATUS                      =   %%PATHFINDER_SLACK%%

; Discord API integration =========================================================================
[PATHFINDER.DISCORD]
; Discord API status
;           This is a global toggle for all Discord related features.
;           Check PATHFINDER.MAP section for individual control.
; Syntax:   0 | 1
; Default:  1
STATUS                      =   %%PATHFINDER_DISCORD%%

; View ============================================================================================
[PATHFINDER.VIEW]
; Page templates
; Hint:     You should not change this.
INDEX                       =   templates/view/index.html
SETUP                       =   templates/view/setup.html
LOGIN                       =   templates/view/login.html
ADMIN                       =   templates/view/admin.html

; HTTP status pages ===============================================================================
[PATHFINDER.STATUS]
; Error page templates
; Hint:     You should not change this.
4XX                         =   templates/status/4xx.html
5XX                         =   templates/status/5xx.html

; MAP =============================================================================================
; Map settings for 'private', 'corporation' and 'alliance' maps:
; LIFETIME (days)
;           - Map will be deleted after 'X' days, by cronjob
; MAX_COUNT
;           - Users can create/view up to 'X' maps of a type
; MAX_SHARED
;           - Max number of shared entities per map
; MAX_SYSTEMS
;           - Max number of active systems per map
; LOG_ACTIVITY_ENABLED (Syntax: 0 | 1)
;           - Whether user activity statistics can be enabled for a map type
;           - E.g. create/update/delete of systems/connections/signatures/...
; LOG_HISTORY_ENABLED (Syntax: 0 | 1)
;           - Whether map change history should be logged to separate *.log files
;           - see: [PATHFINDER.HISTORY] config section below
; SEND_HISTORY_SLACK_ENABLED (Syntax: 0 | 1)
;           - Send map updates to a Slack channel per map
; SEND_RALLY_SLACK_ENABLED (Syntax: 0 | 1)
;           - Send rally point pokes to a Slack channel per map
; SEND_HISTORY_DISCORD_ENABLED (Syntax: 0 | 1)
;           - Send map updates to a Discord channel per map
; SEND_RALLY_DISCORD_ENABLED (Syntax: 0 | 1)
;           - Send rally point pokes to a Discord channel per map
; SEND_RALLY_Mail_ENABLED (Syntax: 0 | 1)
;           - Send rally point pokes by mail
;           - see: [PATHFINDER.NOTIFICATION] section below
[PATHFINDER.MAP.PRIVATE]
LIFETIME                        =   %%PATHFINDER_MAP_PRIVATE_LIFETIME%%
MAX_COUNT                       =   %%PATHFINDER_MAP_PRIVATE_MAX_COUNT%%
MAX_SHARED                      =   %%PATHFINDER_MAP_PRIVATE_MAX_SHARED%%
MAX_SYSTEMS                     =   %%PATHFINDER_MAP_PRIVATE_MAX_SYSTEMS%%
LOG_ACTIVITY_ENABLED            =   %%PATHFINDER_MAP_PRIVATE_LOG_ACTIVITY_ENABLED%%
LOG_HISTORY_ENABLED             =   %%PATHFINDER_MAP_PRIVATE_LOG_HISTORY_ENABLED%%
SEND_HISTORY_SLACK_ENABLED      =   %%PATHFINDER_MAP_PRIVATE_SEND_HISTORY_SLACK_ENABLED%%
SEND_RALLY_SLACK_ENABLED        =   %%PATHFINDER_MAP_PRIVATE_SEND_RALLY_SLACK_ENABLED%%
SEND_HISTORY_DISCORD_ENABLED    =   %%PATHFINDER_MAP_PRIVATE_SEND_HISTORY_DISCORD_ENABLED%%
SEND_RALLY_DISCORD_ENABLED      =   %%PATHFINDER_MAP_PRIVATE_SEND_RALLY_DISCORD_ENABLED%%
SEND_RALLY_Mail_ENABLED         =   %%PATHFINDER_MAP_PRIVATE_SEND_RALLY_MAIL_ENABLED%%

[PATHFINDER.MAP.CORPORATION]
LIFETIME                        =   %%PATHFINDER_MAP_CORPORATION_LIFETIME%%
MAX_COUNT                       =   %%PATHFINDER_MAP_CORPORATION_MAX_COUNT%%
MAX_SHARED                      =   %%PATHFINDER_MAP_CORPORATION_MAX_SHARED%%
MAX_SYSTEMS                     =   %%PATHFINDER_MAP_CORPORATION_MAX_SYSTEMS%%
LOG_ACTIVITY_ENABLED            =   %%PATHFINDER_MAP_CORPORATION_LOG_ACTIVITY_ENABLED%%
LOG_HISTORY_ENABLED             =   %%PATHFINDER_MAP_CORPORATION_LOG_HISTORY_ENABLED%%
SEND_HISTORY_SLACK_ENABLED      =   %%PATHFINDER_MAP_CORPORATION_SEND_HISTORY_SLACK_ENABLED%%
SEND_RALLY_SLACK_ENABLED        =   %%PATHFINDER_MAP_CORPORATION_SEND_RALLY_SLACK_ENABLED%%
SEND_HISTORY_DISCORD_ENABLED    =   %%PATHFINDER_MAP_CORPORATION_SEND_HISTORY_DISCORD_ENABLED%%
SEND_RALLY_DISCORD_ENABLED      =   %%PATHFINDER_MAP_CORPORATION_SEND_RALLY_DISCORD_ENABLED%%
SEND_RALLY_Mail_ENABLED         =   %%PATHFINDER_MAP_CORPORATION_SEND_RALLY_MAIL_ENABLED%%

[PATHFINDER.MAP.ALLIANCE]
LIFETIME                        =   %%PATHFINDER_MAP_ALLIANCE_LIFETIME%%
MAX_COUNT                       =   %%PATHFINDER_MAP_ALLIANCE_MAX_COUNT%%
MAX_SHARED                      =   %%PATHFINDER_MAP_ALLIANCE_MAX_SHARED%%
MAX_SYSTEMS                     =   %%PATHFINDER_MAP_ALLIANCE_MAX_SYSTEMS%%
LOG_ACTIVITY_ENABLED            =   %%PATHFINDER_MAP_ALLIANCE_LOG_ACTIVITY_ENABLED%%
LOG_HISTORY_ENABLED             =   %%PATHFINDER_MAP_ALLIANCE_LOG_HISTORY_ENABLED%%
SEND_HISTORY_SLACK_ENABLED      =   %%PATHFINDER_MAP_ALLIANCE_SEND_HISTORY_SLACK_ENABLED%%
SEND_RALLY_SLACK_ENABLED        =   %%PATHFINDER_MAP_ALLIANCE_SEND_RALLY_SLACK_ENABLED%%
SEND_HISTORY_DISCORD_ENABLED    =   %%PATHFINDER_MAP_ALLIANCE_SEND_HISTORY_DISCORD_ENABLED%%
SEND_RALLY_DISCORD_ENABLED      =   %%PATHFINDER_MAP_ALLIANCE_SEND_RALLY_DISCORD_ENABLED%%
SEND_RALLY_Mail_ENABLED         =   %%PATHFINDER_MAP_ALLIANCE_SEND_RALLY_MAIL_ENABLED%%

; Route search ====================================================================================
[PATHFINDER.ROUTE]
; Search depth for system route search
;           Recursive search depth for search algorithm.
;           This is only used in case ESIs /route/ API responds with errors and the custom search algorithm is used.
; Hint:     Higher values can lead to high CPU load. If to low, routes might not be found even if exist.
; Syntax:   Integer
; Default:  9000
SEARCH_DEPTH                =   %%PATHFINDER_ROUTE_SEARCH_DEPTH%%

; Initial count of routes that will be checked when a system becomes active
; Syntax:   Integer
; Default:  4
SEARCH_DEFAULT_COUNT        =   %%PATHFINDER_ROUTE_SEARCH_DEFAULT_COUNT%%

; Max count of routes that can be selected in 'route settings' dialog
; Syntax:   Integer
; Default:  6
MAX_DEFAULT_COUNT           =   %%PATHFINDER_ROUTE_MAX_DEFAULT_COUNT%%

; Max count of routes that will be checked (MAX_DEFAULT_COUNT + custom routes)
; Syntax:   Integer
; Default:  8
LIMIT                       =   %%PATHFINDER_ROUTE_LIMIT%%

; Email notifications =============================================================================
[PATHFINDER.NOTIFICATION]
; Email address for rally point pokes
;           Requires SMTP configuration (see environment.ini).
; Hint:     This only makes sens if the installation is restricted to allied groups only.
;           This email address is used for all maps on this installation.
; Syntax:   String
; Default:
RALLY_SET                   =

; TIMER ===========================================================================================
;           Timer values should NOT be changed unless you know what they affect!
; =================================================================================================
[PATHFINDER.TIMER]
; Login time for characters. Users get logged out after X minutes
; Hint:     Set to 0 disables login time and characters stay logged in until Cookie data expires
; Syntax:   Integer (minutes)
; Default:  480
LOGGED                      =   480

; Double click timer
; Syntax:   Integer (milliseconds)
; Default:  250
DBL_CLICK                   =   250

; Time for status change visibility in header
; Syntax:   Integer (milliseconds)
; Default:  5000
PROGRAM_STATUS_VISIBLE      =   5000

[PATHFINDER.TIMER.UPDATE_SERVER_MAP]
; Map data update interval (ajax long polling)
;           This is not used for 'WebSocket' configured installations.
; Syntax:   Integer (milliseconds)
; Default:  5000
DELAY                       =   5000

; Execution limit for map data update request (ajax long polling)
;           Requests that exceed the limit are logged as 'warning'.
; Syntax:   Integer (milliseconds)
; Default:  200
EXECUTION_LIMIT             =   500

[PATHFINDER.TIMER.UPDATE_CLIENT_MAP]
; Execution limit for client side (javascript) map data updates
;           Map data updates that exceed the limit are logged as 'warning'.
; Syntax:   Integer (milliseconds)
; Default:  50
EXECUTION_LIMIT             =   100

[PATHFINDER.TIMER.UPDATE_SERVER_USER_DATA]
; User data update interval (ajax long polling)
;           This is not used for 'WebSocket' configured installations.
; Syntax:   Integer (milliseconds)
; Default:  5000
DELAY                       =   5000

; Execution limit for user data update request (ajax long polling)
;           Requests that exceed the limit are logged as 'warning'.
; Syntax:   Integer (milliseconds)
; Default:  500
EXECUTION_LIMIT             =   1000

; update client user data (milliseconds)
[PATHFINDER.TIMER.UPDATE_CLIENT_USER_DATA]
; Execution limit for client side (javascript) user data updates
;           User data updates that exceed the limit are logged as 'warning'.
; Syntax:   Integer (milliseconds)
; Default:  50
EXECUTION_LIMIT             =   100

; CACHE ===========================================================================================
[PATHFINDER.CACHE]
; Checks "character log" data by cronjob after x seconds
;           If character is ingame offline -> delete "character log"
; Syntax:   Integer (seconds)
; Default:  180
CHARACTER_LOG_INACTIVE      =   180

; Max expire time for cache files
;           Files will be deleted by cronjob afterwards.
;           This setting only affects 'file cache'. Redis installations are not affected by this.
; Syntax:   Integer (seconds)
; Default:  864000 (10d)
EXPIRE_MAX                  =   864000

; Expire time for EOL (end of life) connections
;           EOL connections get auto deleted by cronjob afterwards.
; Syntax:   Integer (seconds)
; Default:  15300 (4h + 15min)
EXPIRE_CONNECTIONS_EOL      =   15300

; Expire time for WH connections
;           WH connections get auto deleted by cronjob afterwards.
;           This can be overwritten for each map in the UI.
; Syntax:   Integer (seconds)
; Default:  172800 (2d)
EXPIRE_CONNECTIONS_WH       =   172800

; Expire time for signatures (inactive systems)
;           Signatures get auto deleted by cronjob afterwards.
;           This can be overwritten for each map in the UI.
; Syntax:   Integer (seconds)
; Default:  259200 (3d)
EXPIRE_SIGNATURES           =   259200

; LOGGING =========================================================================================
; Log file configurations
;           Log files are location in [PATHFINDER]/logs/ dir (see: config.ini)
; Syntax:   String
[PATHFINDER.LOGFILES]
; Error log
ERROR                       =   error
; SSO error log
SSO                         =   sso
; Login info
CHARACTER_LOGIN             =   character_login
; Character access
CHARACTER_ACCESS            =   character_access
; Session warnings (mysql sessions only)
SESSION_SUSPECT             =   session_suspect
; Account deleted
DELETE_ACCOUNT              =   account_delete
; Admin action (e.g. kick, ban)
ADMIN                       =   admin
; TCP socket errors
SOCKET_ERROR                =   socket_error
; debug log for development
DEBUG                       =   debug

[PATHFINDER.HISTORY]
; cache time for parsed history log file data
; Syntax:   Integer (seconds)
; Default:  5
CACHE                       =   5

; File folder for 'history' logs (e.g. map history)
; Syntax:   String
; Default:  history/
LOG                         =   history/

; Max file size for 'history' logs before getting truncated by cronjob
; Syntax:   Integer (MB)
; Default:  2
LOG_SIZE_THRESHOLD          =   2

; log entries (lines) after file getting truncated by cronjob
; Syntax:   Integer
; Default:  1000
LOG_LINES                   =   1000

; ADMIN ===========================================================================================
; "SUPER" admins and additional "CORPORATION" admins can be added here
;[PATHFINDER.ROLES]
;CHARACTER.0.ID = 123456789
;CHARACTER.0.ROLE = SUPER
;CHARACTER.1.ID = 1122334455
;CHARACTER.1.ROLE = CORPORATION

; API =============================================================================================
[PATHFINDER.API]
CCP_IMAGE_SERVER            =   https://images.evetech.net
Z_KILLBOARD                 =   https://zkillboard.com/api
EVEEYE                      =   https://eveeye.com
DOTLAN                      =   %%DOTLAN%%
ANOIK                       =   %%ANOIK%%
EVE_SCOUT                   =   %%EVE_SCOUT%%
; GitHub Developer API
GIT_HUB                     =   https://api.github.com

; EXPERIMENTAL [BETA] =============================================================================
; Use these settings with caution!
; They are currently under testing and might be removed in further releases.
[PATHFINDER.EXPERIMENTS]
; Try to use persistent database connections
;           PDO connections get initialized with ATTR_PERSISTENT => true .
;           http://php.net/manual/en/pdo.connections.php#example-1030
; Hint:     Set 'wait_timeout' to a high value in your my.conf to keep them open
; Syntax:   0 | 1
; Default:  0
PERSISTENT_DB_CONNECTIONS   =   1
