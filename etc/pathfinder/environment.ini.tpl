; Environment Config

[ENVIRONMENT]
; project environment (DEVELOP || PRODUCTION).
; This effects: DB connection, Mail-Server, SSO, ESI configurations in this file
; configuration below
SERVER                      =   %%ENVIRONMENT_SERVER%%

[ENVIRONMENT.DEVELOP]
; path to index.php (Default: leave blank == "auto-detect")
; -> e.g. set /pathfinder if your URL looks like https://www.[YOUR_DOMAIN]/pathfinder (subfolder)
BASE                        =
; deployment URL (e.g. http://localhost)
URL                         =   {{@SCHEME}}://local.pathfinder
; level of debug/error stack trace
DEBUG                       =   %%DEBUG%%
; Pathfinder database
DB_PF_DNS                   =   mysql:host=localhost;port=3306;dbname=
DB_PF_NAME                  =   pathfinder
DB_PF_USER                  =   root
DB_PF_PASS                  =

; Universe data (New Eden) cache DB for ESI API respons
DB_UNIVERSE_DNS             =   mysql:host=localhost;port=3306;dbname=
DB_UNIVERSE_NAME            =   eve_universe
DB_UNIVERSE_USER            =   root
DB_UNIVERSE_PASS            =

; CCP SSO (OAuth2) - visit: https://developers.eveonline.com/applications
CCP_SSO_URL                 =   https://sisilogin.testeveonline.com
CCP_SSO_CLIENT_ID           =
CCP_SSO_SECRET_KEY          =
CCP_SSO_JWK_CLAIM           =   login.eveonline.com
CCP_SSO_DOWNTIME            =   11:00

; CCP ESI API
CCP_ESI_URL                 =   https://esi.evetech.net
CCP_ESI_DATASOURCE          =   singularity
CCP_ESI_SCOPES              =   esi-location.read_online.v1,esi-location.read_location.v1,esi-location.read_ship_type.v1,esi-ui.write_waypoint.v1,esi-ui.open_window.v1,esi-universe.read_structures.v1,esi-corporations.read_corporation_membership.v1,esi-clones.read_clones.v1,esi-characters.read_corporation_roles.v1,esi-search.search_structures.v1
CCP_ESI_SCOPES_ADMIN        =

; SMTP settings (optional)
SMTP_HOST                   =   localhost
SMTP_PORT                   =   25
SMTP_SCHEME                 =   TLS
SMTP_USER                   =   pathfinder
SMTP_PASS                   =   root

SMTP_FROM                   =   pathfinder@localhost.com
SMTP_ERROR                  =   pathfinder@localhost.com

; TCP Socket configuration (optional) (advanced)
SOCKET_HOST                 =   127.0.0.1
SOCKET_PORT                 =   5555


[ENVIRONMENT.PRODUCTION]
; path to index.php (Default: leave blank == "auto-detect")
; -> e.g. set /pathfinder if your URL looks like https://www.[YOUR_DOMAIN]/pathfinder (subfolder)
BASE                        =
; deployment URL (e.g. https://www.pathfinder-w.space)
URL                         =   {{@SCHEME}}://%%DOMAIN%%
; level of debug/error stack trace
DEBUG                       =   %%DEBUG%%
; Pathfinder database
DB_PF_DNS                   =   mysql:host=%%MYSQL_HOST%%;port=%%MYSQL_PORT%%;dbname=
DB_PF_NAME                  =   %%DB_PF_NAME%%
DB_PF_USER                  =   %%DB_PF_USER%%
DB_PF_PASS                  =   %%DB_PF_PASS%%

; Universe data (New Eden) cache DB for ESI API respons
DB_UNIVERSE_DNS             =   mysql:host=%%MYSQL_HOST%%;port=%%MYSQL_PORT%%;dbname=
DB_UNIVERSE_NAME            =   %%DB_UNIVERSE_NAME%%
DB_UNIVERSE_USER            =   %%DB_UNIVERSE_USER%%
DB_UNIVERSE_PASS            =   %%DB_UNIVERSE_PASS%%

; EVE-Online CCP Database export
DB_CCP_DNS                  =   mysql:host=%%MYSQL_HOST%%;port=%%MYSQL_PORT%%;dbname=
DB_CCP_NAME                 =   %%DB_CCP_NAME%%
DB_CCP_USER                 =   %%DB_CCP_USER%%
DB_CCP_PASS                 =   %%DB_CCP_PASS%%

; CCP SSO
CCP_SSO_URL                 =   https://login.eveonline.com
CCP_SSO_CLIENT_ID           =   %%CCP_SSO_CLIENT_ID%%
CCP_SSO_SECRET_KEY          =   %%CCP_SSO_SECRET_KEY%%
CCP_SSO_JWK_CLAIM           =   login.eveonline.com
CCP_SSO_DOWNTIME            =   11:00

; CCP ESI API
CCP_ESI_URL                 =   https://esi.evetech.net
CCP_ESI_DATASOURCE          =   tranquility
CCP_ESI_SCOPES              =   esi-location.read_online.v1,esi-location.read_location.v1,esi-location.read_ship_type.v1,esi-ui.write_waypoint.v1,esi-ui.open_window.v1,esi-universe.read_structures.v1,esi-corporations.read_corporation_membership.v1,esi-clones.read_clones.v1,esi-characters.read_corporation_roles.v1,esi-search.search_structures.v1
CCP_ESI_SCOPES_ADMIN        =

; SMTP settings (optional)
SMTP_HOST                   =   %%SMTP_HOST%%
SMTP_PORT                   =   %%SMTP_PORT%%
SMTP_SCHEME                 =   %%SMTP_SCHEME%%
SMTP_USER                   =   %%SMTP_USER%%
SMTP_PASS                   =   %%SMTP_PASS%%

SMTP_FROM                   =   %%SMTP_FROM%%
SMTP_ERROR                  =   %%SMTP_ERROR%%

; TCP Socket configuration (optional) (advanced)
SOCKET_HOST                 =   %%SOCKET_HOST%%
SOCKET_PORT                 =   %%SOCKET_PORT%%
