%%APPLICATION_DIR%%/logs/*.log
%%APPLICATION_DIR%%/history/map/*.log {
  daily
  missingok
  rotate 14
  compress
  notifempty
}
