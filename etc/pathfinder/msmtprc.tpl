defaults

account        opensmtpd
auth           off
logfile        -
from           %%SMTP_FROM%%
host           %%SMTP_HOST%%

account default : opensmtpd
