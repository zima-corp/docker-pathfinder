{
    "settings": {
      "http": {
        "max_body_size": 104857600
      }
    },
    "access_log": {
        "path": "/dev/stdout",
        "format": "remote_addr=$remote_addr host=$host path=$request_uri method=$method status=$status referrer=$header_referer user_agent='$header_user_agent' length=$body_bytes_sent date='$time_local' request_id=$header_x_debug_request_id"
    },

    "listeners": {
        "*:80": {
            "pass": "routes/pathfinder"
        }
    },

    "routes": {
        "pathfinder": [
            {
                "match": {
                    "uri": [
                        "!/manifest.json",
                        "!/robots.txt",
                        "*.bak",
                        "*.config",
                        "*.csv",
                        "*.dist",
                        "*.engine",
                        "*.feature",
                        "*.graphml",
                        "*.inc",
                        "*.info",
                        "*.install",
                        "*.json",
                        "*.less",
                        "*.lock",
                        "*.log",
                        "*.make",
                        "*.md",
                        "*.module",
                        "*.neon",
                        "*.njk",
                        "*.orig",
                        "*.out",
                        "*.phar",
                        "*.po",
                        "*.profile",
                        "*.properties",
                        "*.queue",
                        "*.rb",
                        "*.sass",
                        "*.save",
                        "*.script",
                        "*.scss",
                        "*.scssc",
                        "*.ser",
                        "*.sh",
                        "*.swo",
                        "*.swp",
                        "*.test",
                        "*.theme",
                        "*.tmpl",
                        "*.tpl",
                        "*.twig",
                        "*.txt",
                        "*.vcl",
                        "*.xcf",
                        "*.xtmpl",
                        "*.yml",
                        "*/.*",
                        "*sql",
                        "*~",
                        "/app/*",
                        "/export/*",
                        "/logs/*",
                        "/sass/*",
                        "/tmp/*",
                        "/vendor/*",
                        "LICENSE",
                        "gulpfile.js"
                    ]
                },
                "action": {
                    "return": 404
                }
            },
            {
                "match": {
                    "uri": [
                        "!/index.php*",
                        "*.php"
                    ]
                },
                "action": {
                    "return": 404
                }
            },
            {
                "action": {
                    "share": [
                        "%%APPLICATION_DIR%%$uri"
                    ],
                    "fallback": {
                        "pass": "applications/pathfinder/index"
                    }
                }
            }
        ]
    },

    "applications": {
        "pathfinder": {
            "user": "%%APPLICATION_USER%%",
            "group": "%%APPLICATION_GROUP%%",
            "type": "php",
            "processes": {
                "max": 10,
                "spare": 3,
                "idle_timeout": 30
            },
            "limits": {
                "timeout": 50,
                "requests": 1000
            },
            "options": {
                "admin": {
                    "apc.shm_size": "64M",
                    "date.timezone": "UTC",
                    "error_reporting": "%%PHP_ERROR_REPORTING%%",
                    "html_errors": "%%PHP_HTML_ERRORS%%",
                    "max_execution_time": "50",
                    "max_input_vars": "5000",
                    "memory_limit": "256M",
                    "post_max_size": "100M",
                    "session.save_handler": "%%PHP_SESSION_SAVE_HANDLER%%",
                    "session.save_path": "%%PHP_SESSION_SAVE_PATH%%",
                    "upload_max_filesize": "100M"
                }
            },
            "targets": {
                "direct": {
                    "root": "%%APPLICATION_DIR%%"
                },
                "index": {
                    "root": "%%APPLICATION_DIR%%",
                    "script": "index.php"
                }
            }
        }
    }
}
